import "./Button.css"

const Button = (props) => {
    return (
        <div className="button" onClick={props.f}><h2>Get other quote</h2></div>
    )
}

export default Button