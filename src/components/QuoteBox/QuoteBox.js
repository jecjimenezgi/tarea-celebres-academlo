import "./QuoteBox.css"
import db from "../../db/quotes.json"
import Button from "../Button/Button"
import { useState } from "react";




const QuoteBox = () => {
    let r = Math.floor(Math.random() * 255);
    let g = Math.floor(Math.random() * 255);
    let b = Math.floor(Math.random() * 255);

    const [bcolor, setBcolor] = useState(`rgb(${r},${g},${b})`);
    
    const containerStyle = {
        "backgroundColor": bcolor,
      };



    function init_quote(){
        let random = Math.floor(Math.random() * db["quotes"].length);
        return db["quotes"][random]
    }

    const [quote, setQuote] = useState(init_quote());
    
    function update_quote(){
        let random = Math.floor(Math.random() * db["quotes"].length);
        setQuote(db["quotes"][random])
        r = Math.floor(Math.random() * 255);
        g = Math.floor(Math.random() * 255);
        b = Math.floor(Math.random() * 255);
        setBcolor(`rgb(${r},${g},${b})`)
    }
    
    return (
    <div className="container" id="main" style={containerStyle}>
    <div className="card">
        <h2 id="quote">"{quote.quote}"</h2>
        <h2 id="author">{quote.author}</h2>
        <Button f={update_quote}></Button>
    </div>
    </div>
    )
}

export default QuoteBox